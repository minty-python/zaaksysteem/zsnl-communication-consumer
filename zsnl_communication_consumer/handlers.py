# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from abc import ABC
from minty import Base
from minty.exceptions import NotFound


class BaseHandler(Base, ABC):
    def get_command_instance(self, event):
        return self.cqrs.get_command_instance(
            event.correlation_id,
            "zsnl_domains.communication",
            event.context,
            event.user_uuid,
        )


class EmailImportHandler(BaseHandler):
    def __init__(self, cqrs):
        self.routing_keys = [
            "zsnl.v2.communication.IncomingEmail.EmailReceived"
        ]
        self.cqrs = cqrs

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.import_email(file_uuid=changes["file"])


class EmailSendHandler(BaseHandler):
    def __init__(self, cqrs):
        self.routing_keys = [
            "zsnl.v2.zsnl_domains_communication.ExternalMessage.ExternalMessageCreated",
            "zsnl.v2.zsnl_communication_http_domains_communication.ExternalMessage.ExternalMessageCreated",
        ]
        self.cqrs = cqrs

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }

        if changes["external_message_type"] != "email":
            self.logger.debug(
                f'Ignoring event: message type is {changes["external_message_type"]}, not email'
            )
            return

        if changes["direction"] != "outgoing":
            self.logger.debug(
                f'Ignoring event: direction is {changes["direction"]}, not outgoing'
            )
            return

        command_instance = self.get_command_instance(event)

        command_instance.send_email(message_uuid=event.entity_id)


class AttachedToMessageHandler(BaseHandler):
    """As a user I want to see preview for an attachment.
    When an attachment is linked to message, a pdf derivative for attachment is generated in background.
    """

    def __init__(self, cqrs):
        self.routing_keys = [
            "zsnl.v2.zsnl_domains_communication.MessageAttachment.AttachedToMessage",
            "zsnl.v2.zsnl_communication_http_domains_communication.MessageAttachment.AttachedToMessage",
        ]
        self.cqrs = cqrs

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }

        try:
            command_instance.generate_pdf_derivative(
                attachment_uuid=changes["attachment_uuid"]
            )
        except KeyError:
            self.logger.exception(
                "Got an 'AttachedToMessage' event without an attachment uuid"
            )
        except NotFound:
            self.logger.exception(
                "Got a 'Not Found' exception while trying to generate PDF derivative"
            )
