# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import minty
import minty.logging.mdc
import os
from .consumers import CommunicationConsumer
from logging.config import fileConfig
from minty.cqrs import CQRS
from minty.infrastructure import InfrastructureFactory
from minty.middleware import AmqpPublisherMiddleware
from minty_amqp.client import AMQPClient
from minty_infra_sqlalchemy import DatabaseTransactionMiddleware
from zsnl_domains import communication

ZS_COMPONENT = "zsnl_communication_consumer"

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = ZS_COMPONENT  # type: ignore
    record.req = minty.logging.mdc.get_mdc()  # type: ignore
    return record


def main():
    fileConfig("logging.conf")

    logging.setLogRecordFactory(log_record_factory)

    minty.STATSD_PREFIX = ".".join(
        [ZS_COMPONENT, "silo", os.environ.get("ZS_SILO_ID", "unknown")]
    )

    infra_factory = InfrastructureFactory(config_file="config.conf")
    cqrs = CQRS(
        domains=[communication],
        infrastructure_factory=infra_factory,
        command_wrapper_middleware=[
            DatabaseTransactionMiddleware("database"),
            AmqpPublisherMiddleware(
                publisher_name="communication_consumer",
                infrastructure_name="amqp",
            ),
        ],
    )

    config = cqrs.infrastructure_factory.get_config(context=None)
    amqp_client = AMQPClient(config, cqrs=cqrs)
    amqp_client.register_consumers([CommunicationConsumer])
    amqp_client.start()


if __name__ == "__main__":
    main()  # pragma: no cover
